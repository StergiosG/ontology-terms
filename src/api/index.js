const url = 'https://www.ebi.ac.uk/ols/api/ontologies/efo/terms';

export const GetTermsWithFetch = async (PropsAbortController) => {
  let RequestTimedOut = false;
  const timeoutId = setTimeout(() => {
    PropsAbortController.abort();
    RequestTimedOut = true;
  }, 7000);
  try {
    const response = await fetch(encodeURI(url),
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        signal: PropsAbortController.signal,
      });
    if (!response.ok) {
      console.log('Get terms Request received a "non 2xx" response, HTTP : ', response.status);
    } else {
      let json = '';
      json = await response.json();
      const TermsArray = json._embedded.terms;
      const TermDataArray = TermsArray.map((term) => {
        let CommaSepparatedSynonyms;
        if (term.synonyms) {
          CommaSepparatedSynonyms = term.synonyms.join(', ');
        } else {
          CommaSepparatedSynonyms = 'No synonym found.';
        }
        let CheckedDescription;
        if (term.description) {
          CheckedDescription = term.description;
        } else {
          CheckedDescription = 'No description found.';
        }
        return {
          id: term.obo_id,
          name: term.label,
          url: term.iri,
          description: CheckedDescription,
          is_obsolete: term.is_obsolete,
          synonyms: CommaSepparatedSynonyms,
        };
      });
      return TermDataArray;
    }
  } catch (error) {
    clearTimeout(timeoutId);
    if (error.name === 'AbortError' && RequestTimedOut === true) {
      console.error('Request Time out : Get terms Request did not receive any response within 7 seconds.');
      return ;
    } else if (error.name !== 'AbortError') {
      console.error('Caught error during Get terms Request : ', error);
      return ;
    }
  }
};
