import React from 'react';
import styles from './App.module.css';
import TermContainer from './TermPage/TermContainer';
import logo from './images/logo.png';

function App() {
  return (
    <div className={styles.App}>
      <header className={styles.OlsHeader}>
        <img className={styles.OlsLogo} src={logo} height={100} width={190} alt="OLS Logo" />
      </header>
      <TermContainer />
    </div>
  );
}

export default App;
