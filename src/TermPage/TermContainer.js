import React, { useState, useEffect } from 'react';
import { GetTermsWithFetch } from '../api';
import Pagination from './Pagination/Pagination';
import TermTable from './TermTable/TermTable';
import TermEntryPicker from './TermEntryPicker/TermEntryPicker';
import styles from './TermContainer.module.css';

const TermContainer = () => {
  const [termArray, setTermArray] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [termsPerPage, setTermsPerPage] = useState(5);
  const indexOfLastTerm = currentPage * termsPerPage;
  const indexOfFirstTerm = indexOfLastTerm - termsPerPage;
  const TermsToDisplay = termArray.slice(indexOfFirstTerm, indexOfLastTerm);

  useEffect(() => {
    const abortController = new AbortController();
    const fetchAPI = async () => {
      setLoading(true);
      const returnedArray = await GetTermsWithFetch(abortController);
      setTermArray(returnedArray || []);
      setLoading(false);
    };
    fetchAPI();
    return () => {
      abortController.abort();
    };
  }, []);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  if (loading) {
    return <div className={styles.AlignCenter}>Loading data ...</div>;
  }
  if (termArray.length === 0) {
    return (
      <div className={styles.AlignCenter}>
        <h3>Oops... Something went wrong !</h3>
        <div>No terms were retrieved !</div>
      </div>
    );
  }
  return (
    <div className={styles.container}>
      <h3 className={styles.Title}>List of all terms in OLS</h3>
      <div className={styles.OptionsContainer}>
        <TermEntryPicker
          className={styles.Padder}
          setTermsPerPage={setTermsPerPage}
          termArrayLength={termArray.length}
          setCurrentPage={setCurrentPage}
        />
        <Pagination
          className={styles.Padder}
          termsPerPage={termsPerPage}
          totalTerms={termArray.length}
          paginate={paginate}
          currentPage={currentPage}
        />
      </div>
      <TermTable TermsToDisplay={TermsToDisplay} />
      <div className={styles.PageInfo}>Showing {indexOfFirstTerm + 1 } to {indexOfLastTerm} of total {termArray.length} term entries </div>
    </div>
  );
};

export default TermContainer;
