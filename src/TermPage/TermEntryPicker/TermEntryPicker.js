import React from 'react';
import styles from './TermEntryPicker.module.css';

const TermEntryPicker = ({ setTermsPerPage, termArrayLength, setCurrentPage }) => (
  <div className={styles.Padder}>
    <span>Show</span>
    <select
      className={styles.DropDown}
      onChange={(e) => {
        setTermsPerPage(e.target.value);
        setCurrentPage(1);
      }}
    >
      <option value="5">5</option>
      <option value="10">10</option>
      <option value={termArrayLength}>All</option>
    </select>
    <span>terms</span>
  </div>
);

export default TermEntryPicker;
