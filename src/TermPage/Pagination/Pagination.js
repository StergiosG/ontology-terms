import React from 'react';
import styles from './Pagination.module.css';

const Pagination = ({ totalTerms, termsPerPage, paginate, currentPage }) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalTerms / termsPerPage); i += 1) { pageNumbers.push(i); }
  return (
    <div className={styles.Padder}>
      <span>Page </span>
      {pageNumbers.map((pageNumber) => (
        <span key={pageNumber} className={styles.MySpan}>
          <a
            href="#/"
            className={pageNumber === currentPage ? styles.ActivePage : styles.InnactivePage}
            onClick={() => paginate(pageNumber)}
          >
            {pageNumber}
          </a>
        </span>
      ))}
    </div>
  );
};

export default Pagination;
