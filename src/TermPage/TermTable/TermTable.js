import React from 'react';
import Term from './Term/Term';
import styles from './TermTable.module.css';

const TermTable = ({ TermsToDisplay }) => (
  <table className={styles.table}>
    <thead>
      <tr>
        <th>Term Name</th>
        <th>Term ID</th>
        <th>Description</th>
        <th>Synonyms</th>
      </tr>
    </thead>
    <tbody>
      {TermsToDisplay.map((term) => (
        <Term term={term} key={term.id} />
      ))}
    </tbody>
  </table>
);

export default TermTable;
