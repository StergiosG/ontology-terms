import React from 'react';
import styles from './Term.module.css';

const Term = ({ term }) => (
  <tr>
    <td data-label="Term Name">
      <a href={term.url}>{term.name}</a>
    </td>
    <td data-label="Term ID">
      <span className={styles.TermID}>{term.id}</span>
    </td>
    <td data-label="Description">
      <p className={styles.TextJustify}>{term.description}</p>
    </td>
    <td data-label="Synonyms">
      <div className={styles.TextJustify}>{term.synonyms}</div>
    </td>
  </tr>
);

export default Term;
