import React from 'react';
import { render, screen } from '@testing-library/react';
import Term from '../src/TermPage/TermTable/Term/Term';

test('Term renders properly', () => {
    const Myterm ={
        name : "Bile Reflux", 
        id:"EFO:1000838",
         description:"test description", 
         synonyms:"test Synomym"
      }
  render(<Term term={Myterm} />);
  screen.debug();
  expect(screen.getByText('Bile Reflux')).toBeInTheDocument();
  expect(screen.getByText('EFO:1000838')).toBeInTheDocument();
  expect(screen.getByText('test description')).toBeInTheDocument();
  expect(screen.getByText('test Synomym')).toBeInTheDocument();
});